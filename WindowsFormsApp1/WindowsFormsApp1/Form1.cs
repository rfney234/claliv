﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        SqlConnection connection = new SqlConnection("server = localhost;database=demo;integrated security = true");
        private void Form1_Load(object sender, EventArgs e)
        {
            loadform();
        }
        int listmax = 127,listmin=102;
        string sqlselect  ;
        public void loadform()
        {
            SqlDataAdapter date = new SqlDataAdapter("select Material.Title, MinCount , CountInStock , Cost, Image, MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID where Material.ID >= " + listmin + "and Material.ID <= " + listmax ,connection);
            DataSet set = new DataSet();
            date.Fill(set);
            dataGridView1.DataSource = set.Tables[0];
            label3.Text = "15 из 100";
            string asd = "50";
            for(int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (Convert.ToInt32(dataGridView1.Rows[i].Cells[1].Value) < Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value))
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
                else
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listmax = 127;
            listmin = 102;
            loadform();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            listmax = 135;
            listmin = 120;
            loadform();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            listmax = 150;
            listmin = 135;
            loadform();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            listmax = 175;
            listmin = 150;
            loadform();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlselect1 = "";
            switch (comboBox2.Text)
            {
                case "Все типы":
                    loadform(); listmax = 127;
                    listmin = 102;
                    break;
                case "Гранулы":
                    sqlselect1 = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID where MaterialType.Title = 'Гранулы'";
                    break;
                case "Рулон":
                    sqlselect1 = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID where MaterialType.Title = 'Рулон'";
                    break;
                case "Нарезка":
                    sqlselect1 = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID where MaterialType.Title = 'Нарезка'";
                    break;
                case "Пресс":
                    sqlselect1 = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID where MaterialType.Title = 'Пресс'";
                    break;
                default:
                    loadform(); listmax = 127;
                    listmin = 102;
                    break;

            }
            SqlDataAdapter s = new SqlDataAdapter(sqlselect1, connection);
            DataSet d = new DataSet();
            s.Fill(d);
            dataGridView1.DataSource = d.Tables[0];
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.Text)
            {
                case "По возростанию":
                    sqlselect = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID order by MinCount asc";
                    break;
                case "По убыванию":
                    sqlselect = "select Material.Title , MinCount , Cost , Image , MaterialType.Title  from Material inner join MaterialType on Material.MaterialTypeID = MaterialType.ID order by MinCount desc";
                    break;
                default:
                    loadform();
                    break;
            }
            SqlDataAdapter s = new SqlDataAdapter(sqlselect, connection);
            DataSet d = new DataSet();
            s.Fill(d);
            dataGridView1.DataSource = d.Tables[0];
            label3.Text = "100 из 100";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            listmax = 175;
            listmin = 150;
            loadform();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SqlDataAdapter s = new SqlDataAdapter("select * from Material where Title Like '%" + textBox1.Text + "%'" + "or Cost Like '%" + textBox1.Text + "%'" + "or MinCount Like '%" + textBox1.Text + "%'", connection);
            DataSet d = new DataSet();
            s.Fill(d);
            dataGridView1.DataSource = d.Tables[0];
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            listmax = 190;
            listmin = 175;
            loadform();
        }

      
        
    }
}
