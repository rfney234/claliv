﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace sesia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("server= localhost;database = notepad;integrated security = true;");

        private void Form1_Load(object sender, EventArgs e)
        {
            Update();
            color();
        }
        public void color()
        {
            for(int i=0;i<100;i++)
            {
                if (i % 2 != 0)
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Salmon;
                }
                else
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Gold;
                }
            }
            
        }

        public void Update()
        {
            SqlDataAdapter dat= new SqlDataAdapter("select Material.ID , Material.Title , CountInStock,Unit, MinCount ,Description , Cost , Image , .MaterialType.Title as [Material Title] from Material inner join MaterialType on Material.MaterialTypeID=MaterialType.ID ",con);
            DataSet a = new DataSet();
            dat.Fill(a);
            dataGridView1.DataSource = a.Tables[0];
            color();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
               
                SqlDataAdapter command2 = new SqlDataAdapter("select * from Material where Title " + " Like'%" + textBox1.Text + "%' ", con);

                con.Open();
                DataSet set = new DataSet();
                command2.Fill(set);
                dataGridView1.DataSource = set.Tables[0];
                con.Close();
              
            }
            catch
            {
                MessageBox.Show("Ошибка");textBox1.Text = " ";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Вы действительно хотите удалить данные?");
                SqlCommand comm = new SqlCommand("delete Material where ID = " + textBox2.Text, con);
                comm.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Удаление не возможно (");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var add = new add();
            add.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Update();
            color();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var loli = new loli();
            loli.Show();
            Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBox1.Text)
            {
                case "От большего к меньшему":
                    SqlDataAdapter dat = new SqlDataAdapter("select Material.ID , Material.Title , CountInStock,Unit, MinCount ,Description , Cost , Image , .MaterialType.Title as [Material Title] from Material inner join MaterialType on Material.MaterialTypeID=MaterialType.ID order by CountinStock desc ", con);
                    DataSet a = new DataSet();
                    dat.Fill(a);
                    dataGridView1.DataSource = a.Tables[0];
                    color();
                    break;
                case "От меньшего к большему":
                    SqlDataAdapter d = new SqlDataAdapter("select Material.ID , Material.Title , CountInStock,Unit, MinCount ,Description , Cost , Image , .MaterialType.Title as [Material Title] from Material inner join MaterialType on Material.MaterialTypeID=MaterialType.ID order by CountinStock asc ", con);
                    DataSet a1 = new DataSet();
                    d.Fill(a1);
                    dataGridView1.DataSource = a1.Tables[0];
                    color();
                    break;
                default: Update();color();break;

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (comboBox2.Text)
            {


                case "Фильтрация по цене":
                    SqlDataAdapter dat = new SqlDataAdapter("select Material.ID , Material.Title , CountInStock,Unit, MinCount ,Description , Cost , Image , .MaterialType.Title as [Material Title] from Material inner join MaterialType on Material.MaterialTypeID=MaterialType.ID order by Cost asc ", con);
                    DataSet a = new DataSet();
                    dat.Fill(a);
                    dataGridView1.DataSource = a.Tables[0];
                    color();
                    break;
                case "Фильтрация по минимальному количеству":
                    SqlDataAdapter da = new SqlDataAdapter("select Material.ID , Material.Title , CountInStock,Unit, MinCount ,Description , Cost , Image , .MaterialType.Title as [Material Title] from Material inner join MaterialType on Material.MaterialTypeID=MaterialType.ID order by MinCount asc ", con);
                    DataSet a1 = new DataSet();
                    da.Fill(a1);
                    dataGridView1.DataSource = a1.Tables[0];
                    color();
                    break;
                default: Update(); color(); break;
               
            }
        }
    }
}
