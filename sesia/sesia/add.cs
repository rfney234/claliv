﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sesia
{
    public partial class add : Form
    {
        public add()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection("server= localhost;database = notepad;integrated security = true;");
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand command = new SqlCommand("insert into Material (Title , CountInStock,Unit, MinCount ,Description , Cost , Image , MaterialTypeId ) Values(@1,@2,@3,@4,@5,@6,@7,@8) ", con);
                con.Open();

                command.Parameters.AddWithValue("@1", textBox1.Text);
                command.Parameters.AddWithValue("@2", textBox2.Text);
                command.Parameters.AddWithValue("@3", textBox3.Text);
                command.Parameters.AddWithValue("@4", textBox4.Text);
                command.Parameters.AddWithValue("@5", textBox5.Text);
                command.Parameters.AddWithValue("@6", textBox6.Text);
                command.Parameters.AddWithValue("@7", "картиночки");
                command.Parameters.AddWithValue("@8", textBox7.Text);
                command.ExecuteNonQuery();

            }
            catch {
                MessageBox.Show("Ошибка , вы указываете не верные данные");
                var a = new Form1();
                a.Show();
                Hide();
            }
           


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var a = new Form1();
            a.Show();
            Hide();
        }
    }
}
